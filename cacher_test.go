package cacher

import (
	"testing"
	"time"
)

func TestBasicUse(t *testing.T) {

	th := &TestHelper{}

	cache, err := New(
		th.getStatsRecorder(),
	)
	if err != nil {
		t.Error(err)
	}

	cache.Get("key")

	th.checkStat(t, "request", 1)
	th.checkStat(t, "miss", 1)
	th.checkStat(t, "load", 0)
	th.checkStat(t, "set", 0)
	th.checkStat(t, "hit", 0)

	cache.Set("key", CachedStruct{A: "value"})

	th.checkStat(t, "request", 1)
	th.checkStat(t, "miss", 1)
	th.checkStat(t, "load", 0)
	th.checkStat(t, "set", 1)
	th.checkStat(t, "hit", 0)

	cache.Get("key")

	th.checkStat(t, "request", 2)
	th.checkStat(t, "miss", 1)
	th.checkStat(t, "load", 0)
	th.checkStat(t, "set", 1)

}

func TestCacheZeroValue(t *testing.T) {
	cache, _ := New(
		WithSingleLoader(func(k string) (string, error) {
			return "", nil
		}),
	)

	val, _ := cache.Get("key")

	if val != "" {
		t.Error("expected empty but got", val)
	}
}

func TestDynamicExpireTime(t *testing.T) {
	th := &TestHelper{}

	cache, err := th.initCache(
		WithDynamicExpireTime[string, CachedStruct](func(v CachedStruct, _ error) time.Duration { return v.T * time.Millisecond }),
		th.getStatsRecorder(),
	)
	if err != nil {
		t.Error(err)
	}

	cache.Set("A", CachedStruct{A: "A", T: 100})
	cache.Set("B", CachedStruct{A: "B", T: 200})

	th.get(t, "A").checkValues()
	th.get(t, "B").checkValues()

	time.Sleep(80 * time.Millisecond)

	th.get(t, "A").checkValues()
	th.get(t, "B").checkValues()

	time.Sleep(40 * time.Millisecond)

	th.get(t, "A").checkErr(NotFound)
	th.get(t, "B").checkValues()
}

func TestAutoExpire(t *testing.T) {
	th := &TestHelper{}

	cache, _ := th.initCache(
		WithAutoExpireInterval[string, CachedStruct](time.Millisecond*30),
		WithDefaultExpireTime[string, CachedStruct](20*time.Millisecond),
		th.getStatsRecorder(),
	)

	cache.Set("A", CachedStruct{A: "A"})
	cache.Set("B", CachedStruct{A: "B"})

	th.checkStat(t, "set", 2)
	th.checkStat(t, "expire", 0)

	time.Sleep(25 * time.Millisecond)

	th.get(t, "A").checkErr(NotFound)
	th.checkStat(t, "miss", 1)
	th.checkStat(t, "expire", 0) //Key missed but not yet expired because not removed

	time.Sleep(10 * time.Millisecond)
	th.checkStat(t, "expire", 2)

}
