package cacher

import (
	"context"
	"errors"
	"sync"
	"sync/atomic"
	"time"
)

type CacherError string

const NotFound = CacherError("not found")

const LoadedNil = CacherError("loader func returned nil")

func (e CacherError) Error() string {
	return string(e)
}

var _ Cacher[string, string] = &cacher[string, string]{}
var _ CacherS[string, string] = &cacher[string, string]{}
var _ CacherM[string, string] = &cacher[string, string]{}

type cacher[K comparable, V any] struct {
	mx sync.Mutex

	content map[K]*CacheItem[K, V]

	loaderFunc    func([]K) (map[K]V, map[K]error)
	eventRecorder EventsRecorder[K]

	defaultRefreshTimeFn     ItemOption[K, V]
	defaultExpireTimeFn      ItemOption[K, V]
	defaultErrorExpireTimeFn ItemOption[K, V]

	nowait bool

	cacheContext       context.Context
	cancelContext      context.CancelFunc
	enableLoop         bool
	autoExpireInterval time.Duration

	shouldErrorOverride func(error) bool

	canLoad bool

	autoExpireTicker *time.Ticker
}

func New[K comparable, V any](options ...CacherOption[K, V]) (*cacher[K, V], error) {
	ctx, cancel := context.WithCancel(context.Background())
	c := &cacher[K, V]{
		content:             make(map[K]*CacheItem[K, V]),
		cacheContext:        ctx,
		cancelContext:       cancel,
		eventRecorder:       &NoOpEventsRecorder[K]{},
		defaultExpireTimeFn: noopItemOption[K, V],
	}

	for _, option := range options {
		option(c)
	}

	if c.defaultRefreshTimeFn != nil && c.loaderFunc == nil {
		return nil, errors.New("loaderFunc is required if refreshTime is set")
	}

	if c.eventRecorder == nil {
		c.eventRecorder = &NoOpEventsRecorder[K]{}
	}

	if c.defaultRefreshTimeFn == nil {
		c.defaultRefreshTimeFn = noopItemOption[K, V]
	}

	if c.autoExpireInterval != 0 {
		if c.autoExpireInterval < 0 {
			return nil, errors.New("autoExpireInterval cannot be negative")
		}
		c.autoExpireTicker = time.NewTicker(c.autoExpireInterval)
		c.enableLoop = true
	}

	if c.shouldErrorOverride == nil {
		c.shouldErrorOverride = func(_ error) bool { return false }
	}
	// If no default Error expire time provided, by default apply one of one millisecond
	if c.defaultErrorExpireTimeFn == nil {
		WithDefaultErrorExpireime[K, V](time.Millisecond)(c)
	}

	c.canLoad = c.loaderFunc != nil

	if c.enableLoop {
		go c.loop()
	}

	return c, nil
}

func (c *cacher[K, V]) loop() {
	for {
		select {
		case <-c.cacheContext.Done():
			return
		case <-c.autoExpireTicker.C:
			c.Prune()
		}
	}
}

func (c *cacher[K, V]) Stop() {

	if c.autoExpireTicker != nil {
		c.autoExpireTicker.Stop()
	}
	c.cancelContext()

}

func (cache *cacher[K, V]) Prune() {
	cache.mx.Lock()
	defer cache.mx.Unlock()
	expiredKeys := make([]K, 0)
	for key, item := range cache.content {
		item.readMx.Lock()
		if item.isExpired() && !item.loadState.Load() {
			expiredKeys = append(expiredKeys, key)
		}
		item.readMx.Unlock()
	}
	for _, key := range expiredKeys {
		cache.eventRecorder.KeyExpire(key)
		delete(cache.content, key)
	}
}

func (c *cacher[K, V]) Delete(key K) {
	c.mx.Lock()
	defer c.mx.Unlock()
	delete(c.content, key)
}

func (c *cacher[K, V]) Set(key K, value V) {
	c.SetWithOptions(key, value)
}

func (c *cacher[K, V]) SetWithOptions(key K, value V, options ...ItemOption[K, V]) {
	ci := c.getCacheItem(key)
	ci.readMx.Lock()
	defer ci.readMx.Unlock()
	c.eventRecorder.KeySet(key)
	ci.Value = value
	ci.Err = nil
	ci.isValueSet = true
	ci.writedAt = time.Now()
	c.defaultExpireTimeFn(ci)
	c.defaultRefreshTimeFn(ci)
	for _, opt := range options {
		opt(ci)
	}
}

func (cache *cacher[K, V]) Get(key K) (V, error) {
	k, v := cache.GetMany(key)
	return k[key], v[key]
}

func (cache *cacher[K, V]) GetMany(keys ...K) (map[K]V, map[K]error) {

	//We first get all the cacheItems
	cacheItems := cache.getManyCacheItem(keys)

	values := make(map[K]V, len(cacheItems))
	errs := make(map[K]error, len(cacheItems))

	toLoad := make(map[K]*CacheItem[K, V], 0)
	toRefresh := make(map[K]*CacheItem[K, V], 0)
	//Filter available keys
	for k, ci := range cacheItems {
		ci.readMx.RLock()
		cache.eventRecorder.KeyRequest(k)
		if ci.isReady() {
			cache.eventRecorder.KeyHit(k)
			values[k] = ci.Value
			if ci.Err != nil {
				errs[k] = ci.Err
			}
			if ci.isRefreshTime() {
				toRefresh[k] = ci
			}
		} else {
			cache.eventRecorder.KeyMiss(k)
			if cache.nowait {
				toRefresh[k] = ci
				errs[k] = NotFound
			} else {
				toLoad[k] = ci
			}
		}
		ci.readMx.RUnlock()
	}

	if !cache.canLoad {
		for k := range toLoad {
			errs[k] = NotFound
		}
		return values, errs
	}

	if len(toLoad) > 0 {
		cache.multiLoadCis(toLoad, false)

		for k, v := range toLoad {
			v.readMx.RLock()
			values[k] = v.Value
			errs[k] = v.Err
			v.readMx.RUnlock()
		}
	}
	//Trigger all refreshes
	if len(toRefresh) > 0 {
		go cache.multiLoadCis(toRefresh, true)
	}
	return values, errs
}

func (c *cacher[K, V]) GetAll() (map[K]V, map[K]error) {
	c.mx.Lock()
	defer c.mx.Unlock()
	all := make(map[K]V, len(c.content))
	errs := make(map[K]error, len(c.content))
	for k, ci := range c.content {
		ci.readMx.RLock()
		if !ci.isExpired() {
			all[k] = ci.Value
			errs[k] = ci.Err
		}
		ci.readMx.RUnlock()
	}
	return all, errs
}

func (cache *cacher[K, V]) multiLoadCis(cacheItems map[K]*CacheItem[K, V], refreshOnly bool) {
	currentlyLoading := make([]K, 0)
	toLoad := make([]K, 0)
	doneLoadFuncs := make([]func(), 0)
	//try lock for load
	for _, ci := range cacheItems {

		bc, ack := ci.RequestLoading()
		if ack {
			cache.eventRecorder.KeyLoad(ci.Key)
			toLoad = append(toLoad, ci.Key)
			doneLoadFuncs = append(doneLoadFuncs, bc)
		} else {
			currentlyLoading = append(currentlyLoading, ci.Key)
		}
	}

	//load keys that are not currently loading
	if len(toLoad) > 0 {
		loaded, errs := cache.loaderFunc(toLoad)
		n := time.Now()
		for _, k := range toLoad {
			err := errs[k]
			if refreshOnly && err != nil && !cache.shouldErrorOverride(err) {
				continue
			}
			ci := cacheItems[k]
			ci.readMx.Lock()
			ci.Value = loaded[k]
			ci.Err = errs[k]
			ci.isValueSet = true
			ci.writedAt = n
			cache.defaultExpireTimeFn(ci)
			cache.defaultRefreshTimeFn(ci)
			cache.defaultErrorExpireTimeFn(ci)
			ci.readMx.Unlock()
		}
	}

	for _, f := range doneLoadFuncs {
		f()
	}

	if refreshOnly {
		return
	}

	//wait for the other keys
	for _, key := range currentlyLoading {
		cache.eventRecorder.KeyWait(key)
		cacheItems[key].WaitLoad()
	}
}

func (cache *cacher[K, V]) getCacheItem(key K) *CacheItem[K, V] {
	cis := cache.getManyCacheItem([]K{key})
	return cis[key]
}

func (cache *cacher[K, V]) getManyCacheItem(keys []K) map[K]*CacheItem[K, V] {
	cache.mx.Lock()
	defer cache.mx.Unlock()
	cis := make(map[K]*CacheItem[K, V], len(keys))
	for _, key := range keys {
		ci, ok := cache.content[key]
		if !ok {
			ci = &CacheItem[K, V]{
				Key:      key,
				waitCond: sync.NewCond(&sync.Mutex{}),
			}
			cache.content[key] = ci
		}
		cis[key] = ci
	}
	return cis
}

type CacheItem[K, V any] struct {
	//Used for read and write protection
	readMx sync.RWMutex
	//Used for concurrent load protection
	loadState atomic.Bool
	waitCond  *sync.Cond

	Key        K
	Value      V
	Err        error
	isValueSet bool

	writedAt  time.Time
	refreshAt time.Time
	expireAt  time.Time
}

func (ci *CacheItem[K, V]) RequestLoading() (func(), bool) {

	authorized := ci.loadState.CompareAndSwap(false, true)

	finishedfunc := func() {
		ci.loadState.Store(false)
		ci.waitCond.Broadcast()
	}

	return finishedfunc, authorized
}

func (ci *CacheItem[K, V]) WaitLoad() {
	if !ci.loadState.Load() {
		return
	}
	ci.waitCond.L.Lock()
	//Only lock if currently waiting to avoid waiting when the function has already finished loading
	if ci.loadState.Load() {
		ci.waitCond.Wait()
	}
	ci.waitCond.L.Unlock()
}

func (ci *CacheItem[K, V]) isReady() bool {
	return ci.isValueSet && !ci.isExpired()
}

func (ci *CacheItem[K, V]) isExpired() bool {
	return !ci.expireAt.IsZero() && ci.expireAt.Before(time.Now())
}

func (ci *CacheItem[K, V]) isRefreshTime() bool {
	return !ci.refreshAt.IsZero() && ci.refreshAt.Before(time.Now()) && !ci.isExpired()
}
