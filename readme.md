# Cacher


## What is it

Cacher is a (hopefully) simple caching library that should seamlessly integrate into code where caching could be beneficial.

It's main purpose is the use of loading methods so the caching mechanisme itself is abstracted when retrieving data. You setup the cache, and call "Get".

The focus point are :
 * No dependencies, starting with go 1.18
 * Loading methods
 * Stampeed/Thundering Herd protection for high concurrency apps
 * Expire and Refresh period support, static or computed per cached item
 * Simple methods to retrieve data, that can easily be used behind interfaces
 * Retrieve multiple items at the same time.


What it's not for :
* Straightforward caching without loading methods, it works, but it's not its intended purpose. If you only need "Get/Set", other library might be better for this.
* Very large ammount of data. It does not yet support "complex" LFU/LRU eviction mechanisme, only expiration time.

## Why?

I searched for caching library that supported both generics and loading mechanism and I couldn't find one, so I made one.

### Expire and Refresh ?

The cache has two time period that can be set for each item, the expire time and the refresh time.

When an item has passed it's expire time, the item will be considered as not present anymore, and wont be used. The cache will load the value again and wait for said value to be retrieved to be used.

When an item has passed it's refresh time, the cache will consider the item as present and will use the current value in the cache to be returned immediately. But the cache will also immediately start loading the value to replace it in the cache to avoid the latency of the loading time if the item reach it's expire time. 

### No context ?

No. In the first versions I experimented with passing a context passed to the loader function through "Get", but it was not optimal as the context could block retrieving items, while other callers could have gotten the result without hitting the timeout. Also for passing debug info. traceId or similar, this could be interesting, but I don't see a good way to handle this when multiple request end up waiting for the same value to be loaded.

In the end it's up to the user to handle timeout around the cache. This also help you set higher timeout when loading data, while having much shorter timeout on the process calling the cache.

### Stampeed/Thundering Herd protection

When an item is expired, the cache will load the value again. If the cache is under high load, it could happen that multiple request are waiting for the same value to be loaded. The cache will only call the loader function once, and all other request will wait for the value to be available.

There are lot of ressources existing on the internet about this.  


# Example / how to use

## Examples 
### Basic usage

The main focus is using this cache with loading method. So the simplest example would be having a cache that when called return a new string based on the key.

```go
func generateGreet(name string) (string, error) {
	return fmt.Sprintf("Hello %s!", name), nil
}

greetingCache, _ := cacher.New[string, string](
	cacher.WithSingleLoader(generateGreet),
)
log.Println(greetingCache.Get("Alice")) // Will print "Hello Alice! nil" 
log.Println(greetingCache.Get("Bob")) // Will print "Hello Bob! nil"

```

Now whenever you request greetingCache with any name, if he doesn't have a corresponding value in its cache, it will automaticaly call the method generateGreet and cache its result.

The idea is to have a method that is intensive in compute or io bound (file or HTTP), and that you expect to not change behind the cache to benefit from not having to redo the operation.

### Using options

You can customize the way the cache behave by using options when creating the cache. The options are listed bellow, but the most commonly used should be the one configuring the loader functions, refresh and expire time.

For example, you might want to have a cache above items retrieved from a database, that would be considered as valid for 5 minutes, but after 4 minutes you might want to retrieve the item in the background to avoid at maximum the latency. The cache should automaticaly  Also you might have to retrieve multiple items at the same time from the cache, so it would be better to use a loader function that can return multiple items at once.

```go
func retrieveMultiFromDatabase(keys []string) (map[string]string, map[string]error) {
	//This is a simple example, but you would have a database query here
	result := make(map[string]string)
	errors := make(map[string]error)
	for _, key := range keys {
		result[key] = fmt.Sprintf("Hello %s!", key)
	}
	return result, errors
}

itemCache := cacher.New[string, string](
	cacher.WithMultiLoader(retrieveMultiFromDatabase),
	cacher.WithDefaultExpireTime(5 * time.Minute),
	cacher.WithDefaultRefreshTime(4 * time.Minute),
	cacher.WithAutoExpireInterval(1 * time.Minute),
)

values, errs := itemCache.GetMay(firstKey,secondKey)

```



## Available options

There are several options available that you can use when initializing.

This part should be up to date, but the best way to know what is available is to look at the comments on the functions directly (accessible through go doc).


### WithSingleLoader

Set a loader function that will allow the cache to retrieve the item for a corresponding value one by one
This function will be called if :
 * the item is not found in the cache
 * the item is expired (require an expiration time)
 * the item is within refresh time (require a refresh time)


### WithMultiLoader
Set a loader function that will allow the cache to retrieve multiple item at the same time.
The function should return a map of key and value, and a map of key and error. No value and no error is considered a valid result. The cache will return the zero value and no error.
This function will be called for the same reason described for the WithSingleLoader.
The function should return a map of key and value, and a map of key and error.
For a given key of the list, no value returned in the map will be considered as zero value, and no error will be considered as no error. So if no values and no error are present for a key, the cache will interpret this as having returned the zero value as value and no error.

### WithAutoExpireInterval
Set the interval at wich the cache will automaticaly prune expired items.
This will enable a ticker internaly to the cache that will be stopped when calling the "Stop" method on the cache

### WithDefaultRefreshTime
Set the default refresh time applied to items added to the cache
This option REQUIRE to use a loader function


### WithDefaultExpireTime
Set the default expire time applied to items added to the cache
After an item is expired, it is considered as if it was not present when retrieving the item.


### WithDefaultErrorExpireime
Set the default expire time when an error is returned by the loader function. By default the cache has a one millisecond expire time on error.
This will also override the default refresh time.


### WithErrorOverrideRefresh
By default, when an error is returned by the loader function when refreshing, the returned value will be ignored and the previous value will be kept.
This option change the behavior so an error returne will overwrite the current value in the cache.

### WithErrorOverrideRefreshFunc
Same as WithErrorOverrideRefresh but use the provided function to determine if the error should be used to override the current value in the cache.
The function should return true if the error should be used to override the current value in the cache.

### WithDynamicExpireTime
Allow you to set a custom function to determine the expired duration after an item was set.
This allow you to have different expiration time based on the value of the item.
The value returned will be also be used when an error is returned by a loader function.

### WithNoWaitLoad
Set the cache to not wait for the loader function to return the value.
If the item is not present in the cache, the cache will return a NotFound error and start the loading process in background as if it was a refresh.
This option will only work with a loader function set

### WithEventRecorder
Set an event recorder that will be notified on different signal from the cache


## Event recorders

Event recorder is a simple interface that allow you to be notified of different event from the cache.

This tool is mostly used for debugging, testing or monitoring purposes.

Currently, the following events are triggered :
 * Request : When a key is requested
 * Hit :  When a value requested is available in the cache
 * Miss : When a loader function return a value
 * Set : When a value is set in the cache
 * Load : When a loader function is called
 * WaitLoad : When a loader function is called and the cache is waiting for the value to be returned
 * Expire : When a value is expired during a prune

A BasicEventsRecorder is available that will count how many time each event are triggered using atomic Int64.

You can implement your own event recorder by providing a struct implementing the interface and passing it to the cache using WithEventRecorderOption.

Only the key are provided to the event recorder.


