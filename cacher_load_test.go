package cacher

import (
	"strconv"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestBaseLoader(t *testing.T) {

	th := &TestHelper{}

	_, err := th.initCache(
		(th.getLoaderFunc(time.Millisecond * 10)),
		th.getStatsRecorder(),
	)

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	th.get(t, "key").checkValues().withinMillisDelta(10, 2)
	th.get(t, "key").checkValues().withinMillisDelta(0, 2)
	th.checkCallCount(t, 1)

	th.get(t, "key2").checkValues().withinMillisDelta(10, 2)
	th.checkCallCount(t, 2)

	th.checkStat(t, "request", 3)
	th.checkStat(t, "load", 2)
}

func TestExpire(t *testing.T) {

	th := &TestHelper{}
	th.initCache(
		th.getLoaderFunc(time.Millisecond*30),
		WithDefaultExpireTime[string, CachedStruct](time.Millisecond*10),
	)

	th.get(t, "key").checkValues().withinMillisDelta(30, 2)
	th.get(t, "key").checkValues().withinMillisDelta(0, 2)
	th.checkCallCount(t, 1)
	time.Sleep(20 * time.Millisecond)
	th.get(t, "key").checkValues().withinMillisDelta(30, 2)
	th.checkCallCount(t, 2)
}

func TestRefresh(t *testing.T) {

	th := &TestHelper{}

	th.initCache(
		th.getLoaderFunc(10*time.Millisecond),
		WithDefaultRefreshTime[string, CachedStruct](time.Millisecond*10),
	)

	th.get(t, "key").checkValues().withinMillisDelta(10, 2)
	th.get(t, "key").checkValues().withinMillisDelta(0, 2)
	th.checkCallCount(t, 1)
	//Wait for refresh period
	time.Sleep(20 * time.Millisecond)
	th.get(t, "key").checkValues().withinMillisDelta(0, 2)
	time.Sleep(20 * time.Millisecond)

	th.checkCallCount(t, 2)
}

func TestExpireAndRefresh(t *testing.T) {

	th := &TestHelper{}

	th.initCache(
		th.getLoaderFunc(10*time.Millisecond),
		WithDefaultExpireTime[string, CachedStruct](time.Millisecond*30),
		WithDefaultRefreshTime[string, CachedStruct](time.Millisecond*10),
		th.getStatsRecorder(),
	)

	th.get(t, "key").checkValues().withinMillisDelta(10, 2)
	th.get(t, "key").checkValues().withinMillisDelta(0, 2)
	th.checkCallCount(t, 1)

	time.Sleep(20 * time.Millisecond)

	//Refresh period, return instantly
	th.get(t, "key").checkValues().withinMillisDelta(0, 2)

	time.Sleep(time.Millisecond)
	th.checkCallCount(t, 2)
	time.Sleep(20 * time.Millisecond)

	//Still refresh period
	th.get(t, "key").checkValues().withinMillisDelta(0, 2)
	time.Sleep(time.Millisecond)
	th.checkCallCount(t, 3)
	time.Sleep(40 * time.Millisecond)

	//Expire period, should take longer
	th.get(t, "key").checkValues().withinMillisDelta(10, 2)
	th.checkCallCount(t, 4)

	th.checkStat(t, "hit", 3)
	th.checkStat(t, "miss", 2)
	th.checkStat(t, "load", 4)
	th.checkStat(t, "request", 5)

}

func TestConcurrentLoading(t *testing.T) {
	th := &TestHelper{}
	th.initCache(
		(th.getLoaderFunc(100 * time.Millisecond)),
	)

	go func() { th.get(t, "key").checkValues().withinMillisDelta(100, 5) }()
	go func() { th.get(t, "key").checkValues().withinMillisDelta(100, 5) }()
	time.Sleep(30 * time.Millisecond)
	go func() { th.get(t, "key").checkValues().withinMillisDelta(70, 5) }()
	time.Sleep(30 * time.Millisecond)
	go func() { th.get(t, "key").checkValues().withinMillisDelta(40, 5) }()

	time.Sleep(100 * time.Millisecond)

	th.get(t, "key").checkValues().withinMillisDelta(0, 5)

	th.checkCallCount(t, 1)
}

func TestConcurrentLoadGet(t *testing.T) {
	th := &TestHelper{}

	th.initCache(
		(th.getLoaderFunc(10 * time.Millisecond)),
	)

	go th.get(t, "key").checkValues()
	go th.get(t, "key").checkValues()
	go th.get(t, "key").checkValues()
	go th.get(t, "key").checkValues()
	time.Sleep(20 * time.Millisecond)

	th.checkCallCount(t, 1)

}

func TestParallelLoading(t *testing.T) {
	th := &TestHelper{}
	cache, _ := New(
		th.getStatsRecorder(),
		WithSingleLoader(func(k string) (CachedStruct, error) {
			t, _ := strconv.Atoi(k)
			time.Sleep(time.Duration(t) * time.Millisecond)
			return CachedStruct{A: k}, nil
		}),
	)
	//
	var loaded200, loaded100, failed atomic.Bool
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		t.Log("getting 200")
		cache.Get("200")
		t.Log("got 200")
		if !loaded100.Load() {
			t.Log("100 should have been loaded")
			failed.Store(true)
		}
		loaded200.Store(true)
	}()
	time.Sleep(50 * time.Millisecond)
	go func() {
		defer wg.Done()
		if loaded200.Load() {
			t.Log("200 was already set before calling 100")
			failed.Store(true)
		}
		t.Log("getting 100")
		cache.Get("100")
		t.Log("got 100")
		if loaded200.Load() {
			t.Log("200 was already set")
			failed.Store(true)
		}
		loaded100.Store(true)
	}()
	wg.Wait()

	if failed.Load() {
		t.Error("failed")
	}

}

func TestNoWaitReturn(t *testing.T) {
	th := &TestHelper{}
	th.initCache(
		th.getStatsRecorder(),
		th.getLoaderFunc(10*time.Millisecond),
		WithNoWaitLoad[string, CachedStruct](),
	)
	th.get(t, "key").checkErr(NotFound).withinMillisDelta(0, 2)
	th.get(t, "key").checkErr(NotFound).withinMillisDelta(0, 2)
	time.Sleep(5 * time.Millisecond)
	th.get(t, "key").checkErr(NotFound).withinMillisDelta(0, 2)
	time.Sleep(6 * time.Millisecond)
	th.get(t, "key").checkValues().withinMillisDelta(0, 2)
	th.checkStat(t, "load", 1)
}

func TestErrorLoadExpire(t *testing.T) {

	th := &TestHelper{}
	th.initCache(
		th.getLoaderFunc(10*time.Millisecond),
		WithDefaultExpireTime[string, CachedStruct](20*time.Millisecond),
		WithDefaultErrorExpireime[string, CachedStruct](5*time.Millisecond),
	)

	th.get(t, "error").withinMillisDelta(10, 2).checkErr(errTest)
	th.get(t, "error").withinMillisDelta(0, 2).checkErr(errTest)
	time.Sleep(8 * time.Millisecond)
	th.get(t, "error").withinMillisDelta(10, 2).checkErr(errTest)
	th.get(t, "error").withinMillisDelta(0, 2).checkErr(errTest)
	th.get(t, "ok").withinMillisDelta(10, 2).checkValues()
	th.get(t, "ok").withinMillisDelta(0, 2).checkValues()
	time.Sleep(15 * time.Millisecond)
	th.get(t, "ok").withinMillisDelta(0, 2).checkValues()

}

func TestErrorRefresh(t *testing.T) {
	th := &TestHelper{}
	errRet := atomic.Bool{}
	key := "TestRefreshError"
	th.initCache(
		WithSingleLoader(func(k string) (CachedStruct, error) {
			th.lfcalls.Add(1)
			time.Sleep(10 * time.Millisecond)
			if errRet.Load() {
				return CachedStruct{}, errTest
			}
			return CachedStruct{A: k}, nil
		}),
		WithDefaultExpireTime[string, CachedStruct](200*time.Millisecond),
		WithDefaultRefreshTime[string, CachedStruct](10*time.Millisecond),
		WithDefaultErrorExpireime[string, CachedStruct](15*time.Millisecond),
	)

	// Test OK Key retrieval and cache hit
	th.get(t, key).withinMillisDelta(10, 2).checkValues()
	th.get(t, key).withinMillisDelta(0, 2).checkValues()
	th.checkCallCount(t, 1)

	time.Sleep(20 * time.Millisecond)
	// Test trigger refresh and return OK value from refresh
	// This now reset timing so from this moment, after 210 the key will be expired (10ms of load time + 200 of expire)
	tstart := time.Now()
	th.get(t, key).withinMillisDelta(0, 2).checkValues()
	th.get(t, key).withinMillisDelta(0, 2).checkValues()
	time.Sleep(20 * time.Millisecond) // T+ 20ms
	th.checkCallCount(t, 2)
	errRet.Store(true)
	// Test trigger refresh and return OK value from previous calls
	th.get(t, key).withinMillisDelta(0, 2).checkValues()
	time.Sleep(15 * time.Millisecond) // T+ 35ms
	th.checkCallCount(t, 3)

	//Jump directly to 20 millisecond before refresh
	time.Sleep(160 * time.Millisecond) //T + 195ms
	th.checkCallCount(t, 3)
	th.get(t, key).withinMillisDelta(0, 2).checkValues()
	th.get(t, key).withinMillisDelta(0, 2).checkValues()
	time.Sleep(10 * time.Millisecond) //T + 205ms
	th.checkCallCount(t, 4)
	th.get(t, key).withinMillisDelta(0, 2).checkValues()
	th.get(t, key).withinMillisDelta(0, 2).checkValues()
	time.Sleep(15 * time.Millisecond) //T + 220ms
	th.checkCallCount(t, 5)
	//Now it should act as an expired key
	t.Log(time.Since(tstart))
	th.get(t, key).withinMillisDelta(10, 2).checkErr(errTest)
	th.get(t, key).withinMillisDelta(0, 2).checkErr(errTest)
	th.get(t, key).withinMillisDelta(0, 2).checkErr(errTest)
	th.checkCallCount(t, 6)
}

func TestErrorRefresOverride(t *testing.T) {
	th := &TestHelper{}
	errRet := atomic.Bool{}
	key := "TestRefreshError"
	th.initCache(
		WithSingleLoader(func(k string) (CachedStruct, error) {
			time.Sleep(10 * time.Millisecond)
			if errRet.Load() {
				return CachedStruct{}, errTest
			}
			return CachedStruct{A: k}, nil
		}),
		WithDefaultExpireTime[string, CachedStruct](time.Minute),
		WithDefaultRefreshTime[string, CachedStruct](10*time.Millisecond),
		WithDefaultErrorExpireime[string, CachedStruct](10*time.Millisecond),
		WithErrorOverrideRefresh[string, CachedStruct](),
	)

	// Test OK Key retrieval and cache hit
	th.get(t, key).withinMillisDelta(10, 2).checkValues()
	th.get(t, key).withinMillisDelta(0, 2).checkValues()
	time.Sleep(20 * time.Millisecond)
	// Test trigger refresh and return OK value from refresh
	th.get(t, key).withinMillisDelta(0, 2).checkValues()
	th.get(t, key).withinMillisDelta(0, 2).checkValues()
	time.Sleep(20 * time.Millisecond)
	errRet.Store(true)
	// Test trigger refresh and return OK value from previous calls
	th.get(t, key).withinMillisDelta(0, 2).checkValues()
	time.Sleep(15 * time.Millisecond)
	//Should have refreshed and returned error
	th.get(t, key).withinMillisDelta(0, 2).checkErr(errTest)
	time.Sleep(10 * time.Millisecond)
	errRet.Store(false)
	th.get(t, key).withinMillisDelta(10, 2).checkValues()
	th.get(t, key).withinMillisDelta(0, 2).checkValues()
}
