package cacher

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func BenchmarkGetMiss(b *testing.B) {
	cache, _ := New[string, string]()

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if v, _ := cache.Get("key"); v != "" {
			b.Errorf("expected nothing but got %s", v)
		}
	}
}

func BenchmarkGet(b *testing.B) {
	cache, _ := New[string, string]()
	cache.Set("key", "value")

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if v, _ := cache.Get("key"); v != "value" {
			b.Errorf("expected value but got %s", v)
		}
	}
}

func BenchmarkGetWithExpire(b *testing.B) {
	cache, _ := New(
		WithDefaultExpireTime[string, string](time.Millisecond * 100),
	)
	cache.Set("key", "value")

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		cache.Get("key")
	}
}

func BenchmarkGetWithExpireAndLoader(b *testing.B) {
	cache, _ := New(
		WithSingleLoader(func(k string) (string, error) { return k, nil }),
		WithDefaultExpireTime[string, string](time.Millisecond*100),
	)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if v, _ := cache.Get("key"); v != "key" {
			b.Errorf("expected key but got %s", v)
		}
	}

}
func BenchmarkGetWithRefresh(b *testing.B) {
	cache, _ := New(
		WithSingleLoader(func(k string) (string, error) { return k, nil }),
		WithDefaultRefreshTime[string, string](time.Millisecond*100),
	)

	cache.Get("key")

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if v, _ := cache.Get("key"); v != "key" {
			b.Errorf("expected key but got %s", v)
		}
	}
}

func BenchmarkGetWithExpireAndRefresh(b *testing.B) {
	cache, _ := New(
		WithSingleLoader(func(k string) (string, error) {
			return k, nil
		}),
		WithDefaultExpireTime[string, string](time.Millisecond*200),
		WithDefaultRefreshTime[string, string](time.Millisecond*100),
	)

	cache.Get("key")

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if v, _ := cache.Get("key"); v != "key" {
			b.Errorf("expected key but got %s", v)
		}
	}
}

func BenchmarkGetMany(b *testing.B) {
	b.Run("1", GetManyBench(1))
	b.Run("2", GetManyBench(2))
	b.Run("4", GetManyBench(4))
	b.Run("8", GetManyBench(8))
	b.Run("16", GetManyBench(16))
}

func GetManyBench(count int) func(b *testing.B) {
	return func(b *testing.B) {
		cache, _ := New[string, string]()
		keyMap := make(map[string]string)
		keys := make([]string, count)
		for i := 0; i < count; i++ {
			key := fmt.Sprintf("key-%d", i)
			value := fmt.Sprintf("value-%d", i)
			keyMap[key] = value
			cache.Set(key, value)
			keys[i] = key
		}

		b.ResetTimer()

		for i := 0; i < b.N; i++ {
			v, _ := cache.GetMany(keys...)
			for k, value := range keyMap {
				if v[k] != value {
					b.Errorf("expected %s but got %s", value, v[k])
				}
			}
		}
	}
}

func BenchmarkGetManyWithRefreshAndExpire(b *testing.B) {
	b.Run("1", GetManyWithRefreshAndExpire(1))
	b.Run("2", GetManyWithRefreshAndExpire(2))
	b.Run("4", GetManyWithRefreshAndExpire(4))
	b.Run("8", GetManyWithRefreshAndExpire(8))
	b.Run("16", GetManyWithRefreshAndExpire(16))
	b.Run("64", GetManyWithRefreshAndExpire(64))
	b.Run("128", GetManyWithRefreshAndExpire(128))
}

func GetManyWithRefreshAndExpire(count int) func(b *testing.B) {
	return func(b *testing.B) {
		cache, _ := New(
			WithSingleLoader(func(k string) (string, error) {
				return k, nil
			}),
			WithDefaultExpireTime[string, string](time.Second*200),
			WithDefaultRefreshTime[string, string](time.Second*100),
		)
		keys := make([]string, count)
		for i := 0; i < count; i++ {
			key := fmt.Sprintf("key-%d", i)
			keys[i] = key
		}

		b.ResetTimer()

		for i := 0; i < b.N; i++ {
			v, _ := cache.GetMany(keys...)
			for _, k := range keys {
				if v[k] != k {
					b.Errorf("expected %s but got %s", k, v[k])
				}
			}
		}
	}
}

func BenchmarkWithExpireRandomOrder(b *testing.B) {
	er := &BasicEventsRecorder[string]{}
	cache, _ := New(
		WithSingleLoader(func(k string) (string, error) {
			time.Sleep(time.Millisecond * 2)
			return k, nil
		}),
		WithEventRecorder[string, string](er),
		WithDefaultExpireTime[string, string](time.Millisecond*10),
	)

	b.ResetTimer()

	b.SetParallelism(64)
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			keys := make([]string, 10)
			for i := 0; i < 10; i++ {
				keys[i] = fmt.Sprintf("key-%d", rand.Intn(1000))
			}
			vals, _ := cache.GetMany(keys...)
			for _, k := range keys {
				if vals[k] != k {
					b.Errorf("expected %s but got %s", k, vals[k])
				}
			}
		}
	})

	b.Logf("request %d, wait %d, hit %d, load %d  wait + hit + load %d \n", er.KeyRequestCount.Load(), er.KeyWaitLoadCount.Load(), er.KeyHitCount.Load(), er.KeyLoadCount.Load(), er.KeyWaitLoadCount.Load()+er.KeyHitCount.Load()+er.KeyLoadCount.Load())

}

func BenchmarkWithRefreshRandomOrder(b *testing.B) {
	er := &BasicEventsRecorder[string]{}
	cache, _ := New(
		WithSingleLoader(func(k string) (string, error) {
			time.Sleep(time.Millisecond * 2)
			return k, nil
		}),
		WithEventRecorder[string, string](er),
		WithDefaultExpireTime[string, string](time.Millisecond*10),
		WithDefaultRefreshTime[string, string](5*time.Millisecond),
	)

	b.ResetTimer()

	b.SetParallelism(64)
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			keys := make([]string, 10)
			for i := 0; i < 10; i++ {
				keys[i] = fmt.Sprintf("key-%d", rand.Intn(1000))
			}
			vals, _ := cache.GetMany(keys...)
			for _, k := range keys {
				if vals[k] != k {
					b.Errorf("expected %s but got %s", k, vals[k])
				}
			}
		}
	})

	b.Logf("request %d, wait %d, hit %d, load %d  wait + hit + load %d \n", er.KeyRequestCount.Load(), er.KeyWaitLoadCount.Load(), er.KeyHitCount.Load(), er.KeyLoadCount.Load(), er.KeyWaitLoadCount.Load()+er.KeyHitCount.Load()+er.KeyLoadCount.Load())

}

func BenchmarkWithExpireNoWait(b *testing.B) {
	er := &BasicEventsRecorder[string]{}
	cache, _ := New(
		WithSingleLoader(func(k string) (string, error) {
			time.Sleep(time.Millisecond * 2)
			return k, nil
		}),
		WithEventRecorder[string, string](er),
		WithDefaultExpireTime[string, string](time.Millisecond*10),
		WithNoWaitLoad[string, string](),
	)

	b.ResetTimer()

	b.SetParallelism(64)
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			keys := make([]string, 10)
			for i := 0; i < 10; i++ {
				keys[i] = fmt.Sprintf("key-%d", rand.Intn(1000))
			}
			vals, _ := cache.GetMany(keys...)
			for _, k := range keys {
				v := vals[k]
				if v != "" && v != k {
					b.Errorf("expected %s but got %s", k, vals[k])
				}
			}
		}
	})

	b.Logf("request %d, miss %d, hit %d, load %d  hit + miss %d \n", er.KeyRequestCount.Load(), er.KeyMissCount.Load(), er.KeyHitCount.Load(), er.KeyLoadCount.Load(), er.KeyMissCount.Load()+er.KeyHitCount.Load())

}
