package cacher

import (
	"testing"
	"time"
)

func TestRecurciveCall(t *testing.T) {
	var fiboCache Cacher[int, int64]

	fiboCache, _ = New[int, int64](WithSingleLoader(func(n int) (int64, error) {
		if n < 2 {
			return int64(n), nil
		}
		n1, _ := fiboCache.Get(n - 1)
		n2, _ := fiboCache.Get(n - 2)
		return n1 + n2, nil
	}))
	tstart := time.Now()
	if v, _ := fiboCache.Get(40); v != 102334155 {
		t.Errorf("Expected 102334155, got %d", v)
	}
	t.Logf("First time taken: %v", time.Since(tstart))

	tstart = time.Now()
	if v, _ := fiboCache.Get(40); v != 102334155 {
		t.Errorf("Expected 102334155, got %d", v)
	}
	t.Logf("Second time taken: %v", time.Since(tstart))

}
