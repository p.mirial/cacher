package cacher

// This is the cacher interface with all the methods that can be used to interact with the cache.
type Cacher[K comparable, V any] interface {

	// Get retrieves the value associated with the key.
	// See the different options for the possible behavior of Get key
	// If used with a loader function, get can call the loader function if the key is not found. Get will be waiting for response from loader func (unless nowait function is used)
	// If the loader function returns an error, get will return the error.
	// If no loader function, or nowait option is used, then get can return a NotFound error if the key is not found.
	// If the item exist but is past its refresh time but not yet expired, the value will immediately be returned, and the loading function will be called to refresh the item
	Get(K) (V, error)

	// GetMany will retrieve multiple values associated with the keys.
	// It will work on the same principle as Get, but for multiple keys.
	// It is avised to use this With Multi Loader even if not required.
	GetMany(...K) (map[K]V, map[K]error)

	// GetAll will return all pair of key and value currently in the cache.
	GetAll() (map[K]V, map[K]error)

	// Set will directly set a value in the cache for a specified key
	Set(K, V)

	// SetWithOptions has overall the same behavior as Set, but you can pass additional options to it.
	SetWithOptions(K, V, ...ItemOption[K, V])

	//Delete will completly remove a key from the cache
	Delete(K)

	// Stop will stop all functions of the cache. Only call this if you discard the cache, as things may break and paning if calling Stop and cache is still used
	Stop()

	// Manually trigger a pruning of keys in the cache.
	Prune()
}

// This is the cacher interface with only the Get Method
type CacherS[K comparable, V any] interface {
	// Get retrieves the value associated with the key.
	Get(K) (V, error)
}

// This is the cacher interface with only Get and GetMany
type CacherM[K comparable, V any] interface {
	Get(K) (V, error)

	GetMany(...K) (map[K]V, map[K]error)
}

type EventsRecorder[K comparable] interface {
	KeyRequest(key K)
	KeyHit(key K)
	KeyMiss(key K)
	KeySet(key K)
	KeyLoad(key K)
	KeyWait(key K)
	KeyExpire(key K)
}

//embedability check
