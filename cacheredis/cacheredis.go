package cacheredis

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/redis/go-redis/v9"
)

type CacherRedis[K comparable, V any] struct {
	Cli       redis.UniversalClient
	KeyFormat string
	Encoder   func(V) ([]byte, error)
	Decoder   func([]byte) (V, error)
	TTL       time.Duration
}

func (c CacherRedis[K, V]) Wrapper(f func([]K) (map[K]V, map[K]error)) func([]K) (map[K]V, map[K]error) {
	c.prep()
	return func(keys []K) (map[K]V, map[K]error) {
		valsRedis, _ := c.get(keys)
		missing := make([]K, 0, len(keys))
		for _, k := range keys {
			if _, ok := valsRedis[k]; !ok {
				missing = append(missing, k)
			}
		}
		var errors map[K]error
		if len(missing) > 0 {

			vals, errsFunc := f(missing)
			for _, k := range missing {
				if err := errsFunc[k]; err == nil {
					delete(errsFunc, k)
				}
			}
			c.set(vals)

			for k, v := range vals {
				valsRedis[k] = v
			}
			errors = errsFunc
		}

		return valsRedis, errors
	}
}

func (c CacherRedis[K, V]) WrapperSingle(f func(K) (V, error)) func(K) (V, error) {
	c.prep()
	return func(key K) (V, error) {
		valsRedis, _ := c.get([]K{key})
		if v, ok := valsRedis[key]; ok {
			return v, nil
		}
		val, err := f(key)
		if err != nil {
			return val, err
		}
		c.set(map[K]V{key: val})
		return val, nil
	}
}

func (c CacherRedis[K, V]) Getter() func([]K) (map[K]V, map[K]error) {
	c.prep()
	return c.get
}

func (c *CacherRedis[K, V]) prep() {
	if c.KeyFormat == "" {
		c.KeyFormat = "%s"
	}
	if c.Decoder == nil {
		c.Decoder = func(payload []byte) (V, error) {
			var val V
			json.Unmarshal(payload, &val)
			return val, nil
		}
	}
	if c.Encoder == nil {
		c.Encoder = func(value V) ([]byte, error) {
			payload, err := json.Marshal(value)
			return payload, err
		}
	}
}

func (c *CacherRedis[K, V]) get(keys []K) (map[K]V, map[K]error) {

	if len(keys) == 0 {
		return nil, nil
	}
	getKeys := make([]string, len(keys))
	for i, k := range keys {
		getKeys[i] = fmt.Sprintf(c.KeyFormat, k)
	}

	redisValues, err := c.Cli.MGet(context.Background(), getKeys...).Result()

	if err != nil {
		var zerovK K
		return nil, map[K]error{zerovK: err}
	}

	result := make(map[K]V, len(keys))
	errs := make(map[K]error, len(keys))

	for i, v := range getKeys {
		if v == "" {
			continue
		}
		rv := redisValues[i]

		if rv == nil {
			errs[keys[i]] = redis.Nil
		}
		rvstr, ok := rv.(string)
		if !ok {
			errs[keys[i]] = fmt.Errorf("value returned by redis is not string")
			continue
		}
		decoded, err := c.Decoder([]byte(rvstr))
		if err != nil {
			errs[keys[i]] = err
			continue
		}
		result[keys[i]] = decoded
		continue

	}

	return result, errs
}

func (c *CacherRedis[K, V]) set(values map[K]V) {

	if len(values) == 0 {
		return
	}
	ctx := context.Background()
	pipe := c.Cli.Pipeline()
	for k, v := range values {
		str, err := c.Encoder(v)
		if err != nil {
			continue
		}
		pipe.Set(ctx, fmt.Sprintf(c.KeyFormat, k), str, c.TTL)
	}

	pipe.Exec(ctx)

}
