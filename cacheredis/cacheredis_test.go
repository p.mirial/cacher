package cacheredis

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/alicebob/miniredis/v2"
	"github.com/redis/go-redis/v9"
)

func TestGetObj(t *testing.T) {

	ctx := context.Background()

	s, _ := miniredis.Run()
	cli := redis.NewClient(&redis.Options{Addr: s.Addr()})

	w := &CacherRedis[string, string]{
		Cli:       cli,
		KeyFormat: "obj:%v",
	}

	vals, errs := w.Getter()([]string{"key"})

	if len(vals) != 0 {
		t.Errorf("expected 0 value, got %d", len(vals))
	}

	if len(errs) != 1 && errs["key"] != redis.Nil {
		t.Errorf("expected 1 error, got %d", len(errs))
	}

	cli.Set(ctx, "obj:key", "\"value\"", 0)

	vals, errs = w.Getter()([]string{"key"})
	if len(vals) != 1 && vals["key"] != "value" {
		t.Errorf("expected 1 value, got %d", len(vals))
	}

	if len(errs) != 0 {
		t.Errorf("expected 0 error, got %d", len(errs))
	}

}

func TestWrapper(t *testing.T) {

	ctx := context.Background()

	s, _ := miniredis.Run()
	cli := redis.NewClient(&redis.Options{Addr: s.Addr()})

	w := &CacherRedis[string, string]{
		Cli:       cli,
		KeyFormat: "obj:%v",
	}

	wrapper := w.Wrapper(func(keys []string) (map[string]string, map[string]error) {
		result := make(map[string]string, len(keys))
		for _, k := range keys {
			result[k] = "value-" + k
		}
		return result, nil
	})

	if cli.Get(ctx, "obj:key").Err() != redis.Nil {
		t.Errorf("expected not found")
	}

	vals, errs := wrapper([]string{"key"})
	if len(vals) != 1 && vals["key"] != "value-key" {
		t.Errorf("expected 1 value, got %d", len(vals))
	}
	if len(errs) != 0 {
		t.Errorf("expected 0 error, got %d", len(errs))
	}

	if v, err := cli.Get(ctx, "obj:key").Result(); err != nil {
		t.Errorf("expected no errors found")
	} else if v != "\"value-key\"" {
		t.Errorf("expected value-key, got %s", v)
	}
}

func TestWrapperObjectAndTTL(t *testing.T) {
	ctx := context.Background()
	count = 0
	s, _ := miniredis.Run()
	cli := redis.NewClient(&redis.Options{Addr: s.Addr()})

	w := &CacherRedis[string, Obj]{
		Cli:       cli,
		KeyFormat: "obj:%v",
		TTL:       100 * time.Millisecond,
	}

	wrapper := w.Wrapper(keysObjs)

	vals, _ := wrapper([]string{"key"})
	if len(vals) != 1 {
		t.Errorf("expected 1 value, got %d", len(vals))
	}
	if vals["key"].Cnt != 1 {
		t.Errorf("expected 1 in Cnt, got %d", vals["key"].Cnt)
	}

	if _, err := cli.Get(ctx, "obj:key").Result(); err != nil {
		t.Errorf("expected no errors found")
	}

	s.FastForward(80 * time.Millisecond)
	vals, _ = wrapper([]string{"key"})

	if len(vals) != 1 {
		t.Errorf("expected 1 value, got %d", len(vals))
	}
	if vals["key"].Cnt != 1 {
		t.Errorf("expected 1 in Cnt, got %d", vals["key"].Cnt)
	}

	s.FastForward(80 * time.Millisecond)

	vals, _ = wrapper([]string{"key"})
	if len(vals) != 1 {
		t.Errorf("expected 1 value, got %d", len(vals))
	}
	if vals["key"].Cnt != 2 {
		t.Errorf("expected 2 in Cnt, got %d", vals["key"].Cnt)
	}
}

func TestWrapperError(t *testing.T) {

	count = 0
	s, _ := miniredis.Run()
	cli := redis.NewClient(&redis.Options{Addr: s.Addr()})

	w := &CacherRedis[string, Obj]{
		Cli:       cli,
		KeyFormat: "obj:%v",
		TTL:       100 * time.Millisecond,
	}

	returnedErr := fmt.Errorf("testError")

	wrapper := w.Wrapper(func(key []string) (map[string]Obj, map[string]error) {
		errs := make(map[string]error, len(key))
		for _, k := range key {
			errs[k] = returnedErr
		}
		return nil, errs
	})

	vals, errs := wrapper([]string{"key"})

	if len(vals) != 0 {
		t.Errorf("expected 0 value, got %d", len(vals))
	}

	if len(errs) != 1 {
		t.Errorf("expected 1 error, got %d", len(errs))
	}
	if errs["key"] != returnedErr {
		t.Errorf("expected returned error, got %v", errs["key"])
	}

}

type Obj struct {
	Txt   string
	Tim   time.Time
	Cnt   int64
	Child *Obj
}

var count int64 = 0
var mx sync.Mutex

func keysObjs(keys []string) (map[string]Obj, map[string]error) {
	result := make(map[string]Obj, len(keys))
	mx.Lock()
	defer mx.Unlock()
	for _, k := range keys {
		count++
		result[k] = Obj{
			Txt: "value-" + k,
			Tim: time.Date(2024, 1, 2, 3, 4, 5, 6, time.UTC),
			Cnt: count,
			Child: &Obj{
				Txt: "child-" + k,
				Tim: time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC),
				Cnt: 69,
			},
		}
	}
	return result, nil
}

type Key struct {
	Type string
	Id   int64
}

func (k Key) String() string {
	return fmt.Sprintf("%s:%d", k.Type, k.Id)
}

func TestString(t *testing.T) {
	k := Key{Type: "type", Id: 1}
	t.Logf("Key: '%s', '%v'", k, k)
}
