package cacher

import (
	"errors"
	"fmt"
	"sync/atomic"
	"testing"
	"time"
)

type CachedStruct struct {
	A string
	T time.Duration
}
type TestHelper struct {
	lfcalls atomic.Int32
	prefix  string
	ber     *BasicEventsRecorder[string]
	cache   *cacher[string, CachedStruct]
}

type callResult struct {
	t        *testing.T
	keys     []string
	values   map[string]CachedStruct
	errs     map[string]error
	duration time.Duration
}

var errTest = errors.New("test error")

func (th *TestHelper) initCache(options ...CacherOption[string, CachedStruct]) (*cacher[string, CachedStruct], error) {
	cache, err := New(options...)
	th.cache = cache
	return cache, err
}

func (th *TestHelper) getLoaderFunc(delay time.Duration) CacherOption[string, CachedStruct] {
	return WithSingleLoader(func(k string) (CachedStruct, error) {
		th.lfcalls.Add(1)
		time.Sleep(delay)
		if k == "error" {
			return CachedStruct{}, errTest
		}
		return CachedStruct{A: fmt.Sprintf("%s%s", th.prefix, k)}, nil
	})
}

func (th *TestHelper) checkCallCount(t *testing.T, expected int) {
	t.Helper()
	if int(th.lfcalls.Load()) != expected {
		t.Errorf("expected %d calls, got %d", expected, th.lfcalls.Load())
	}
}

func (th *TestHelper) getStatsRecorder() CacherOption[string, CachedStruct] {
	th.ber = &BasicEventsRecorder[string]{}
	return WithEventRecorder[string, CachedStruct](th.ber)

}

func (th *TestHelper) checkStat(t *testing.T, key string, expected int64) {

	t.Helper()
	var value int64

	switch key {
	case "request":
		value = th.ber.KeyRequestCount.Load()
	case "hit":
		value = th.ber.KeyHitCount.Load()
	case "miss":
		value = th.ber.KeyMissCount.Load()
	case "set":
		value = th.ber.KeySetCount.Load()
	case "load":
		value = th.ber.KeyLoadCount.Load()
	case "wait":
		value = th.ber.KeyWaitLoadCount.Load()
	case "expire":
		value = th.ber.KeyExpireCount.Load()
	default:
		t.Errorf("unknown key %s", key)
		return
	}
	if value != expected {
		t.Errorf("expected %d %s, got %d", expected, key, value)
	}

}

func (th *TestHelper) get(t *testing.T, keys ...string) callResult {

	tbef := time.Now()
	values, errs := th.cache.GetMany(keys...)
	duration := time.Since(tbef)
	return callResult{t, keys, values, errs, duration}
}

func (cr callResult) checkErr(err error) callResult {
	cr.t.Helper()
	for _, k := range cr.keys {
		if cr.values[k] != (CachedStruct{}) || cr.errs[k] != err {
			cr.t.Errorf("expected nothing and error:'%v' but got value %v and error:'%v'\n", err, cr.values[k], cr.errs[k])
		}
	}
	return cr
}

func (cr callResult) withinMillisDelta(durr, delta time.Duration) callResult {
	cr.t.Helper()
	min, max := (durr-delta)*time.Millisecond, (durr+delta)*time.Millisecond
	if cr.duration < min || cr.duration > max {
		cr.t.Errorf("expected between %s and %s but got %s\n", min, max, cr.duration)
	}
	return cr
}
func (cr callResult) checkValues() callResult {
	cr.t.Helper()
	for _, k := range cr.keys {
		if cr.values[k].A != k {
			cr.t.Errorf("expected %v but got %v\n", k, cr.values[k].A)
		}
	}
	return cr
}
