package cacher

import (
	"sync"
	"testing"
	"time"
)

func TestMulti(t *testing.T) {
	th := &TestHelper{}
	_, err := th.initCache(
		th.getLoaderFunc(time.Millisecond*10),
		th.getStatsRecorder(),
	)
	if err != nil {
		t.Fatal(err)
	}

	th.get(t, "1", "2", "3").checkValues().withinMillisDelta(10, 2)

	th.checkCallCount(t, 3)
	th.checkStat(t, "request", 3)
	th.checkStat(t, "load", 3)

	th.get(t, "1", "2", "3").checkValues().withinMillisDelta(0, 2)
	th.checkCallCount(t, 3)
	th.checkStat(t, "request", 6)
	th.checkStat(t, "load", 3)

	th.get(t, "2", "3", "4").checkValues().withinMillisDelta(10, 2)
	th.checkCallCount(t, 4)
	th.checkStat(t, "request", 9)
	th.checkStat(t, "load", 4)

}

func TestMultiConcurrent(t *testing.T) {
	th := &TestHelper{}
	_, err := th.initCache(
		th.getLoaderFunc(time.Millisecond*100),
		th.getStatsRecorder(),
	)
	if err != nil {
		t.Fatal(err)
	}

	wg := sync.WaitGroup{}
	wg.Add(10)
	for i := 0; i < 10; i++ {
		go func() {
			th.get(t, "1", "2", "3").checkValues().withinMillisDelta(100, 5)
			wg.Done()
		}()
	}
	wg.Wait()

	th.checkCallCount(t, 3)
	th.checkStat(t, "request", 30)
	th.checkStat(t, "load", 3)
}

func TestMultiConcurrentMixed(t *testing.T) {
	th := &TestHelper{}
	_, err := th.initCache(
		th.getLoaderFunc(time.Millisecond*100),
		th.getStatsRecorder(),
	)
	if err != nil {
		t.Fatal(err)
	}

	go func() { th.get(t, "1", "2", "3").checkValues().withinMillisDelta(100, 5) }()
	go func() { th.get(t, "3", "4", "5").checkValues().withinMillisDelta(100, 5) }()
	time.Sleep(110 * time.Millisecond)

	th.checkCallCount(t, 5)
	th.checkStat(t, "request", 6)
	th.checkStat(t, "load", 5)
	th.checkStat(t, "wait", 1)

}

func TestMultiAllCase(t *testing.T) {
	th := &TestHelper{}
	_, err := th.initCache(
		th.getLoaderFunc(time.Millisecond*20),
		th.getStatsRecorder(),
		WithDefaultExpireTime[string, CachedStruct](time.Millisecond*100),
		WithDefaultRefreshTime[string, CachedStruct](time.Millisecond*50),
	)

	if err != nil {
		t.Fatal(err)
	}

	//Setup scenario for a call that will have one available key, one that will trigger a refresh,request one key that is expired, one that is currently refreshing and one unknown key

	// t -130ms so key wil be expired (20 + 100)
	go th.get(t, "expired")
	time.Sleep(75 * time.Millisecond)
	// t -55ms so key will need to be refreshed
	go th.get(t, "toRefresh")
	time.Sleep(30 * time.Millisecond)
	// t -25ms so key will be there
	go th.get(t, "ok")
	time.Sleep(15 * time.Millisecond)
	// t -10ms so key will be refreshing
	go th.get(t, "refreshing")
	time.Sleep(10 * time.Millisecond)
	// t 0ms
	th.get(t, "new", "expired", "refreshing", "toRefresh", "ok").checkValues().withinMillisDelta(20, 2)

	th.checkCallCount(t, 6)
	th.checkStat(t, "request", 9)
	th.checkStat(t, "load", 6)
	th.checkStat(t, "wait", 1)
	th.checkStat(t, "hit", 2)

}

func TestMultiAllCaseNoWait(t *testing.T) {
	th := &TestHelper{}
	_, err := th.initCache(
		th.getLoaderFunc(time.Millisecond*20),
		th.getStatsRecorder(),
		WithDefaultExpireTime[string, CachedStruct](time.Millisecond*100),
		WithDefaultRefreshTime[string, CachedStruct](time.Millisecond*50),
		WithNoWaitLoad[string, CachedStruct](),
	)

	if err != nil {
		t.Fatal(err)
	}

	//Setup scenario for a call that will have one available key, one that will trigger a refresh,request one key that is expired, one that is currently refreshing and one unknown key

	// t -130ms so key wil be expired (20 + 100)
	go th.get(t, "expired")
	time.Sleep(75 * time.Millisecond)
	// t -55ms so key will need to be refreshed
	go th.get(t, "toRefresh")
	time.Sleep(30 * time.Millisecond)
	// t -25ms so key will be there
	go th.get(t, "ok")
	time.Sleep(15 * time.Millisecond)
	// t -10ms so key will be refreshing
	go th.get(t, "refreshing")
	time.Sleep(10 * time.Millisecond)
	// t 0ms
	vals, errs := th.cache.GetMany("new", "expired", "refreshing", "toRefresh", "ok")

	th.checkCallCount(t, 4)

	if vals["new"] != (CachedStruct{}) || errs["new"] != NotFound {
		t.Errorf("expected not found for new but got %s, err %v\n", vals["new"], errs["new"])
	}
	if vals["expired"] != (CachedStruct{}) || errs["expired"] != NotFound {
		t.Errorf("expected not found for expired but got %s, err %v\n", vals["expired"], errs["expired"])
	}
	if vals["refreshing"] != (CachedStruct{}) || errs["refreshing"] != NotFound {
		t.Errorf("expected not found for refreshing but got %s, err %v\n", vals["refreshing"], errs["refreshing"])
	}
	if vals["toRefresh"].A != "toRefresh" || errs["toRefresh"] != nil {
		t.Errorf("expected toRefresh but got %s, err %v\n", vals["toRefresh"], errs["toRefresh"])
	}
	if vals["ok"].A != "ok" || errs["ok"] != nil {
		t.Errorf("expected ok but got %s, err %v\n", vals["ok"], errs["ok"])
	}

	time.Sleep(30 * time.Millisecond)

	th.checkCallCount(t, 6)
	th.checkStat(t, "request", 9)
	th.checkStat(t, "load", 6)
	th.checkStat(t, "wait", 0)
	th.checkStat(t, "hit", 2)

}

func TestMultiSameKey(t *testing.T) {
	th := &TestHelper{}
	_, err := th.initCache(
		th.getLoaderFunc(time.Millisecond*10),
		th.getStatsRecorder(),
	)
	if err != nil {
		t.Fatal(err)
	}

	th.get(t, "1", "1", "1", "2").checkValues()

}
