package cacher

import (
	"sync"
	"time"
)

// Type representing options passed when intianilizing an instance of cacher
type CacherOption[K comparable, V any] func(*cacher[K, V])

// Type representing options passed when getting or setting an item from the cache
type ItemOption[K comparable, V any] func(*CacheItem[K, V])

// Set a loader function that will allow the cache to retrieve the item for a corresponding value one by one
// This function will be called if :
// * the item is not found in the cache
// * the item is expired (require an expiration time)
// * the item is within refresh time (require a refresh time)
func WithSingleLoader[K comparable, V any](loader func(K) (V, error)) CacherOption[K, V] {
	return func(c *cacher[K, V]) {
		if c.loaderFunc == nil {
			c.loaderFunc = singleToMultiAdapter(loader)
		}
	}
}

// Set a loader function that will allow the cache to retrieve multiple item at the same time.
// The function should return a map of key and value, and a map of key and error. No value and no error is considered a valid result. The cache will return the zero value and no error.
// This function will be called for the same reason described for the WithSingleLoader.

func WithMultiLoader[K comparable, V any](multiLoader func([]K) (map[K]V, map[K]error)) CacherOption[K, V] {
	return func(c *cacher[K, V]) {
		c.loaderFunc = multiLoader
	}
}

// Set the interval at wich the cache will automaticaly prune expired items.
// This will enable a ticker internaly to the cache, if it need to be stopped, you can do so by calling Stop method, but this should only be donne once the cache isn't used anymore
func WithAutoExpireInterval[K comparable, V any](interval time.Duration) CacherOption[K, V] {
	return func(c *cacher[K, V]) {
		c.autoExpireInterval = interval
	}
}

// Set the default refresh time applied to items added to the cache
// This option REQUIRE to use a loader function
func WithDefaultRefreshTime[K comparable, V any](refreshTime time.Duration) CacherOption[K, V] {
	return func(c *cacher[K, V]) {
		c.defaultRefreshTimeFn = WithRefreshTime[K, V](refreshTime)
	}
}

// Set the default expire time applied to items added to the cache
// After an item is expired, it is considered as if it was not present when retrieving the item.
func WithDefaultExpireTime[K comparable, V any](expireTime time.Duration) CacherOption[K, V] {
	return func(c *cacher[K, V]) {
		c.defaultExpireTimeFn = WithExpireTime[K, V](expireTime)
	}
}

// Set the default expire time when an error is returned by the loader function. By default the cache has a one millisecond expire time on error.
func WithDefaultErrorExpireime[K comparable, V any](errorExpireTime time.Duration) CacherOption[K, V] {
	return func(c *cacher[K, V]) {
		c.defaultErrorExpireTimeFn = func(item *CacheItem[K, V]) {
			if item.Err != nil {
				item.expireAt = item.writedAt.Add(errorExpireTime)
				item.refreshAt = item.writedAt.Add(errorExpireTime * 2)
			}
		}
	}
}

// Set the cache to override the value of an item if an error is returned by the loader function.
func WithErrorOverrideRefresh[K comparable, V any]() CacherOption[K, V] {
	return func(c *cacher[K, V]) {
		c.shouldErrorOverride = func(error) bool { return true }
	}
}

// Set the function that signal the cache if it should override the value of an item if an error is returned by the loader function.
// Should return true if the error needs to override current value.
func WithErrorOverrideRefreshFunc[K comparable, V any](fn func(error) bool) CacherOption[K, V] {
	return func(c *cacher[K, V]) {
		c.shouldErrorOverride = fn
	}
}

// Set a custom function to determine an expire time after the item has been returned.
// This function use used to determine the expire time both when the item is correctly retrieved or an error is returned by a loader function
func WithDynamicExpireTime[K comparable, V any](expireFunc func(V, error) time.Duration) CacherOption[K, V] {
	return func(c *cacher[K, V]) {
		fn := func(item *CacheItem[K, V]) {
			item.expireAt = item.writedAt.Add(expireFunc(item.Value, item.Err))
		}
		c.defaultExpireTimeFn = fn
		c.defaultErrorExpireTimeFn = fn
	}
}

// Set the cache to not wait for the loader function to return the value. If the item is not present in the cache, the cache will return a NotFound error and start the loading process in background.
// This option will only work with a loader function set
func WithNoWaitLoad[K comparable, V any]() CacherOption[K, V] {
	return func(c *cacher[K, V]) {
		c.nowait = true
	}
}

// Set an event recorder that will be notified on different signal from the cache
func WithEventRecorder[K comparable, V any](er EventsRecorder[K]) CacherOption[K, V] {
	return func(c *cacher[K, V]) {
		c.eventRecorder = er
	}
}

// Set the refresh time applied to an item added to the cache.
// Not intended to be used directly by users
func WithRefreshTime[K comparable, V any](refreshTime time.Duration) ItemOption[K, V] {
	return func(item *CacheItem[K, V]) {
		if item.Err == nil {
			item.refreshAt = item.writedAt.Add(refreshTime)
		}
	}
}

// Set the expire time applied to an item added to the cache.
// Not intended to be used directly by users
// Do not use this on cache without loader function. The cache may panic
func WithExpireTime[K comparable, V any](expireTime time.Duration) ItemOption[K, V] {
	return func(item *CacheItem[K, V]) {
		if item.Err == nil {
			item.expireAt = item.writedAt.Add(expireTime)
		}
	}
}

func noopItemOption[K comparable, V any](*CacheItem[K, V]) {}

// Convert a function that load a single item to a function that load multiple items
// When requesting multiple items, each call will be made in parallel using a goroutine
// When requesting only one item, no goroutine will be created and the call will be made directly.
func singleToMultiAdapter[K comparable, V any](loader func(K) (V, error)) func([]K) (map[K]V, map[K]error) {
	return func(keys []K) (map[K]V, map[K]error) {
		//Quick if only one item required
		if len(keys) == 1 {
			k := keys[0]
			v, err := loader(k)
			return map[K]V{k: v}, map[K]error{k: err}
		}
		var wg sync.WaitGroup
		var mu sync.Mutex
		items := make(map[K]V)
		errs := make(map[K]error)
		for _, k := range keys {
			wg.Add(1)
			go func(k K) {
				defer wg.Done()
				v, err := loader(k)
				mu.Lock()
				items[k] = v
				errs[k] = err
				mu.Unlock()
			}(k)
		}
		wg.Wait()
		return items, errs
	}
}
