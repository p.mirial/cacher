package cacher

import "sync/atomic"

var _ EventsRecorder[string] = &NoOpEventsRecorder[string]{}

type NoOpEventsRecorder[K comparable] struct{}

func (*NoOpEventsRecorder[K]) KeyRequest(K) {}
func (*NoOpEventsRecorder[K]) KeyHit(K)     {}
func (*NoOpEventsRecorder[K]) KeyMiss(K)    {}
func (*NoOpEventsRecorder[K]) KeySet(K)     {}
func (*NoOpEventsRecorder[K]) KeyLoad(K)    {}
func (*NoOpEventsRecorder[K]) KeyWait(K)    {}
func (*NoOpEventsRecorder[K]) KeyExpire(K)  {}

var _ EventsRecorder[string] = &BasicEventsRecorder[string]{}

type BasicEventsRecorder[K comparable] struct {
	KeyRequestCount  atomic.Int64
	KeyHitCount      atomic.Int64
	KeyMissCount     atomic.Int64
	KeySetCount      atomic.Int64
	KeyLoadCount     atomic.Int64
	KeyWaitLoadCount atomic.Int64
	KeyExpireCount   atomic.Int64
}

func (er *BasicEventsRecorder[K]) KeyRequest(K) { er.KeyRequestCount.Add(1) }
func (er *BasicEventsRecorder[K]) KeyHit(K)     { er.KeyHitCount.Add(1) }
func (er *BasicEventsRecorder[K]) KeyMiss(K)    { er.KeyMissCount.Add(1) }
func (er *BasicEventsRecorder[K]) KeySet(K)     { er.KeySetCount.Add(1) }
func (er *BasicEventsRecorder[K]) KeyLoad(K)    { er.KeyLoadCount.Add(1) }
func (er *BasicEventsRecorder[K]) KeyWait(K)    { er.KeyWaitLoadCount.Add(1) }
func (er *BasicEventsRecorder[K]) KeyExpire(K)  { er.KeyExpireCount.Add(1) }
